# Trabalho Final de AM2021/1

## Equipe
* Daniel Coimbra dos Santos
* Gabriel Marques Tiveron
* Luis Henrique Pereira Taira
* Renan Cristyan Araujo Pinheiro
* Samuel de Souza Buters Pereira


## Use Cases 

1. Receber o PDF por meio de UX
1. Obter as páginas do PDF (pdf2image/pytorch server)
1. **Para cada página, segmentar a imagem**
1. Apresentar ao OCR (Tesseract) os bounding boxes da imagem
1. Acumular o resultado em texto com a respectiva classe por página
1. Devolver a string e apresenta-la na UX


>xxtitle1 SEÇÃO II

>xxtitle2 SECRETARIA DE ESTADO DE GOVERNO

>xxtitle3 COORDENADORIA DAS CIDADES ...

>xxtitle4 EXTRATO DO CONTRATO ...

>xxtext Processo 145.000.030 ... Carlos Cesar Vieira

>xxtitle4 RATIFICAÇÃO ...

>xxtext Processo: 145 

## Critérios de Avaliação

### Avaliação 1 - Modelo
|Perspectiva|Subpersp.|Detalhe|Percentual|
|---|---|---|---|
|Dados|Estrutura|como o dado está organizado <br> sua distribuição ...?|10%|
|Dados|Preprocessamento|quais técnicas de preproc foram <br> aplicadas ao dado?|10%|
|Dados|Batches & DataLoaders|como o dado é organizado para alimentar <br> a rede? Quais e para que servem os parametros<br>do(s) dataloader(s)<br>Como foi splitado?|20%|
|Modelo|Arquitetura|Qual a arquitetura geral? Quais as <br>ativações de cada bloco? Quais a <br>dimensões de input/output? |20%|
|Modelo|Hiperametros|Quais são? Como foram definidos? Qual <br>o efeito de cada um na rede?|20%|
|Modelo|Métricas|Qual a métrica de sucesso? Como ela é <br>calculada? Como ela opera <br>na função de loss?|20%|

### Avaliação 2 - Serviço
|Perspectiva|Detalhe|Percentual|
|---|---|---|
|Interface| Carregamento do PDF no jupyter| 5% |
|Interface| Apresentar os textos das páginas em uma celula do jupyter| 5% |
|API| Preproc do PDF (pdf2image). Para cada imagem:<br><ul><li>realizar inferência. Para cada boudingbox <br> <ul> <li>OCR Tesseract</li> <li> Acumular o texto </li></ul> </ul><ul><li>acumular o texto para cada imagem </li></ul> <br> devolver o texto como resposta| 70% |
|Modelo|Explicar o modelo usado. Arquitetura, hiperametros (lr, normalization, estratégia de finetune, etc.) |20%|

Exemplo de saída
```
xxpag01

xxtitle1 SEÇÃO II

xxtitle2 SECRETARIA DE ESTADO DE GOVERNO

xxtitle3 COORDENADORIA DAS CIDADES ...

xxtitle4 EXTRATO DO CONTRATO ...

xxtext Processo 145.000.030 ... Carlos Cesar Vieira

xxtitle4 RATIFICAÇÃO ...

xxtext Processo: 145 

xxpag02

xxtitle1 SEÇÃO II

xxtitle2 SECRETARIA DE ESTADO DE GOVERNO

xxtitle3 COORDENADORIA DAS CIDADES ...

xxtitle4 EXTRATO DO CONTRATO ...

xxtext Processo 145.000.030 ... Carlos Cesar Vieira

xxtitle4 RATIFICAÇÃO ...

xxtext Processo: 145 

xxpag03

xxtitle1 SEÇÃO II

xxtitle2 SECRETARIA DE ESTADO DE GOVERNO

xxtitle3 COORDENADORIA DAS CIDADES ...

xxtitle4 EXTRATO DO CONTRATO ...

xxtext Processo 145.000.030 ... Carlos Cesar Vieira

xxtitle4 RATIFICAÇÃO ...

xxtext Processo: 145 

```




